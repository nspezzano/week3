#9/14/15
#Nick Spezzano
#Number Guess Game

import random

keepGoing = True


while keepGoing:
        target = random.randint(1, 100)
        print(target)
        guessesTaken = 0
        guess = 0

        while (guessesTaken <= 4 and target != guess):

                
                guess = int(input("Please type in a number between 1 and 100. "))

                guessesTaken += 1
                                
                if (guess > target):
                        print("Your guess is too high. Try again" )

                if (guess < target):
                        print("Your guess is too low. Try again" )

                if (guess == target):
                        print ("You guessed correctly!")

        if (guess == target):
            guessesTaken = str(guessesTaken)
            print ("You guessed right! You guessed my number in " + guessesTaken + " guesses.")
                             
        if (guess != target):
            target = str(target)
            print("Sorry, you lose.")

        yesOrNo = input ("Play again? Yes or no? ")
        if yesOrNo == ("no" or "No"):
                keepGoing = False
